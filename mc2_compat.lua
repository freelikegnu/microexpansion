-- need to find a subtitute in MC2 for tin and obsidian shards
microexpansion.compat = {
    default = {
        steel_ingot = "mcl_core:iron_ingot",
        copper_ingot = "mcl_copper:copper_ingot",
        tin_ingot = "mcl_copper:copper_ingot",
        obsidian_shard = "mcl_amethyst:amethyst_shard",
        stone = "mcl_core:stone",
        furnace = "mcl_core:furnace",
        chest = "mcl_chests:chest"

    },
    png = {stone = "default_stone.png"},
    names = {steel_infused_obsidian = "iron_infused_amethyst"},
    desc = {steel_infused_obsidian = "Iron Infused Amethyst"}
}
