-- shared/init.lua

local me = microexpansion

-- This mostly contains items that are used by multiple modules and
-- don't really fit with anything else.

-- [register item] Steel Infused Obsidian Ingot
me.register_item(microexpansion.compat.names.steel_infused_obsidian.."_ingot", {
	description = microexpansion.compat.desc.steel_infused_obsidian.." Ingot",
	recipe = {
		{ 2, {
				{ microexpansion.compat.default.steel_ingot, microexpansion.compat.default.obsidian_shard, microexpansion.compat.default.steel_ingot },
			},
		},
	},
})

-- [register item] Machine Casing
me.register_item("machine_casing", {
	description = "Machine Casing",
	recipe = {
		{ 1, {
				{microexpansion.compat.default.steel_ingot, microexpansion.compat.default.steel_ingot, microexpansion.compat.default.steel_ingot},
				{microexpansion.compat.default.steel_ingot, microexpansion.compat.default.copper_ingot, microexpansion.compat.default.steel_ingot},
				{microexpansion.compat.default.steel_ingot, microexpansion.compat.default.steel_ingot, microexpansion.compat.default.steel_ingot},
			},
		},
	},
})
