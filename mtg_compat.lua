microexpansion.compat = {
    default = {
        steel_ingot = "default:steel_ingot",
        copper_ingot = "default:copper_ingot",
        tin_ingot = "default:tin_ingot",
        obsidian_shard = "default:obsidian_shard",
        stone = "default:stone",
        furnace = "default:furnace",
        chest = "default:chest"

    },
    png = {stone = "default_stone.png"},
    names = {steel_infused_obsidian = "steel_infused_obsidian"},
    desc = {steel_infused_obsidian = "Steel Infused Obsidian"}
}
